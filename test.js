function findElements(json, searchElements, depth) {
    let resultArr = []
    let objectToPush = {
        foundElements: [],
        depth: depth
    }
    Object.keys(json).forEach(key => {
        if(json[key] instanceof Object) {
            findElements(json[key], searchElements, depth+1).forEach(obj => {
                resultArr.push(obj);
            })
        } else if (searchElements.includes(key)) {
            objectToPush.foundElements.push(key)
        }
    })
    if(objectToPush.foundElements.length >0) {
        resultArr.push(objectToPush)
    }
    return resultArr;
   
}

//Case 1
const json1 = {
    firstName : "ramen",
    lastName: "sayami",
    sin: "321654987",
    client2: {
        firstName: 'neelajma',
        lastName: 'sthapit',
        sin: "987654",
        client3: {
            firstName: 'mijal',
            lastName: 'shrestha'
        }
    }
}
console.log('Case 1')
console.log(findElements(json1, ['firstName'], 1));

// Case 2
const json2 = {
    lastName: "sayami",
    sin: "321654987",
    client2: {
        firstName: 'neelajma',
        lastName: 'sthapit',
        sin: "987654",
        client3: {
            firstName: 'mijal',
            lastName: 'shrestha'
        }
    }
}
console.log('Case 2')
console.log(findElements(json2, ['firstName'], 1));


// Case 3
const json3 = {
    lastName: "sayami",
    sin: "321654987",
    client2: {
        firstName: 'neelajma',
        sin: "987654",
        client3: {
            firstName: 'mijal',
            lastName: 'shrestha'
        }
    }
}
console.log('Case 3')
console.log(findElements(json3, ['firstName','lastName'], 1));
